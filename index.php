<?php

setlocale(LC_ALL, 'ru_RU.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251'); //Russian locale
$config = dirname(__FILE__) . '/protected/config/main.php';
/** !!DELETE IN PRODUCTION SERVER!!! **/
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);

    defined('YII_DEBUG') or define('YII_DEBUG', true); // remove the following lines when in production mode
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3); // specify how many levels of call stack should be shown in each log message
    $config = dirname(__FILE__) . '/protected/config/develop.php';
}
$yii = dirname(__FILE__) . '/framework/yiilite.php'; // change the following paths if necessary
//$yii = dirname(__FILE__) . '/framework/yii.php';

require_once($yii);
Yii::setPathOfAlias('vendor', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor');
Yii::createWebApplication($config)->run();
?>