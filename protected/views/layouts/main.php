<?php
Yii::app()->clientScript->registerCssFile($this->getPathAssets('/css/main.css'));
Yii::app()->clientScript->registerMetaTag('text/html; charset=utf-8', NULL, 'Content-Type');
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body>
        <?php
            $this->widget(
                    'bootstrap.widgets.TbNavbar', array(
                'type' => 'inverse',
                'brand' => Yii::app()->name,
                'brandUrl' => array('main/index'),
                'collapse' => TRUE, // requires bootstrap-responsive.css
                'fixed' => 'top',
                'fluid' => TRUE, //по ширине экрана
                'items' => array(
                    array(
                        'class' => 'bootstrap.widgets.TbMenu',
                        'htmlOptions' => array('class' => 'pull-right'),
                    ),
                ),
                    ));
        ?>
        <div class="container-fluid">
            <?php echo $content; ?>
            <hr>
            <footer><p>Created &copy; <?php echo date("Y"); ?></p></footer>
        </div>
    </body>
</html>