<?php

/* @var $this MainController */
/* @var $model Users */

$this->pageTitle = Yii::app()->name . ' - Авторизация';
echo CHtml::openTag('div', array('class' => 'row-fluid'));
echo CHtml::openTag('div', array('class' => 'span12', 'style' => 'margin-top:10%;'));

$form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm', array(
    'id' => 'loginForm',
    'htmlOptions' => array('class' => 'well offset4 span4'), // for inset effect
        )
);
echo CHtml::tag('h2', array(), 'Авторизация');
echo $form->textFieldRow($model, 'username', array('class' => 'span12'));
echo $form->passwordFieldRow($model, 'password', array('class' => 'span12'));
$this->widget(
        'bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Войти')
);
$this->endWidget();
echo CHtml::closeTag('div');
echo CHtml::closeTag('div');
?>