<?php

return array(
    //'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'YII DEMO PROJECT',
    'defaultController' => 'Main',
    'preload' => array('bootstrap'), // preloading component
    'import' => array( // autoloading model and component classes
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.helpers.*',
    ),
    'modules' => array(),
    'components' => array( // application components
        'user' => array(
            'allowAutoLogin' => true, // enable cookie-based authentication
            'loginUrl' => array('main/login'),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'urlSuffix' => '.html',
            'showScriptName' => false,
            'rules' => array(
                '' => 'main/index',
                '<action:(index|login|logout|about|contact)>' => 'main/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'bootstrap' => array(
            'class' => 'vendor.clevertech.yii-booster.src.components.Bootstrap',
            'responsiveCss' => true,
        ),
//        'db' => array(
//            'class' => 'system.db.CDbConnection',
//            'connectionString' => 'mysql:host=localhost;dbname=h773_travel',
//            'username' => 'h773_root',
//            'password' => 'root',
//            'charset' => 'utf8',
//            'tablePrefix' => 'tbl_',
//            //'emulatePrepare' => true,
//            'schemaCachingDuration' => 3600,
//        ),
        'cache' => array(
            'class' => 'system.caching.CDummyCache'
            //'class' => 'system.caching.CDbCache'
            //'class' => 'system.caching.CFileCache'
            //'class'=>'system.caching.CApcCache'
            //'class'=>'system.caching.CZendDataCache'
            //'class'=>'system.caching.CXCache'
        ),
        'errorHandler' => array(
            'errorAction' => 'main/error', // action to display errors
        ),
    ),
    'sourceLanguage' => 'en_US',
    'language' => 'ru',
    'charset' => 'utf-8',
);
