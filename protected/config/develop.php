<?php

/**
 * Конфиг разработчика
 */
return CMap::mergeArray(
                // наследуемся от main.php
                require(dirname(__FILE__) . '/main.php'), array(
            'preload' => array('log'), // preloading 'log' component
            'modules' => array(
                'gii' => array(
                    'class' => 'system.gii.GiiModule',
                    'password' => '123',
                    'ipFilters' => array('127.0.0.1', '::1'),
                    'generatorPaths' => array(
                        'bootstrap.gii',
                    ),
                ),
            ),
            'components' => array(
                // переопределяем компонент db
                'db' => array(// настройка соединения с базой
                    'enableProfiling' => true, // включаем профайлер
                    'enableParamLogging' => true, // показываем значения параметров
                    'schemaCachingDuration' => 0,
                ),
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'CProfileLogRoute',
                            'report' => 'summary',
                            'levels' => 'trace, info',
                        //'showInFireBug' => true,
                        //'report' => 'summary',
                        // lists execution time of every marked code block
                        // report can also be set to callstack
                        ),
                        array(
                            'class' => 'CWebLogRoute',
                            //'categories' => 'system.db.*',
                            'except' => 'system.db.*', // показываем всё, что касается базы данных, но не касается AR
                        ),
                    ),
                ),
            ),
                )
);
