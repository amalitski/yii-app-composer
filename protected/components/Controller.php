<?php

class Controller extends CController {
    /**
     * Пусть к папке с временными данными (стили, скрипты, картинки)
     * @return URL
     */
    protected function getPathAssets($pathFileInAssets) {
        return Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.views.assets') . $pathFileInAssets);
    }

}
